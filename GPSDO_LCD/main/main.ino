/*
  LiquidCrystal Library - Pin setup:
  The circuit:
 * LCD RS pin to digital pin 12
 * LCD Enable pin to digital pin 11
 * LCD D4 pin to digital pin 5
 * LCD D5 pin to digital pin 4
 * LCD D6 pin to digital pin 3
 * LCD D7 pin to digital pin 2
 * LCD R/W pin to ground
 * LCD VSS pin to ground
 * LCD VCC pin to 5V
 * 10K resistor:
 * ends to +5V and ground
 * wiper to LCD VO pin (pin 3)
 */
 
// include the library code:
#include <LiquidCrystal.h>
#include <NeoSWSerial.h>
#include "Arduino.h"
#include "minmea.h"
#include <math.h>
#define DALLAS
#ifdef DALLAS
#include <OneWire.h>
#include <DallasTemperature.h>
#endif

NeoSWSerial GPSDOserial(8, 7); // RX, TX

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

#ifdef DALLAS
// nastavení čísla vstupního pinu 
const int pinCidlaDS = 6;
// vytvoření instance oneWireDS z knihovny OneWire
OneWire oneWireDS(pinCidlaDS);
// vytvoření instance senzoryDS z knihovny DallasTemperature
DallasTemperature senzoryDS(&oneWireDS);
#endif



    volatile unsigned int GPSDOcount = 0;
    volatile char GPSDObuffer[512];
    void handleRxChar( uint8_t c )
    {
      if(GPSDOcount < 512)
        GPSDObuffer[GPSDOcount++] = c;
    }
    

void setup() {
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.setCursor(0, 0);
  lcd.print(" GPSDO  Display ");
  lcd.setCursor(0, 1);
  lcd.print("FW v1.1 - OK2NMZ");

  // Open serial communications
  GPSDOserial.attachInterrupt( handleRxChar );
  GPSDOserial.begin( 9600 );

  Serial.begin(115200);
    while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }


#ifdef DALLAS
  // zapnutí komunikace knihovny s teplotním čidlem
  senzoryDS.begin();
#endif
  senzoryDS.requestTemperatures();
  
  delay(1500);
  
  lcd.setCursor(0, 0);
  lcd.print("                ");
  lcd.setCursor(0, 1);
  lcd.print("                ");
}
#define GPSDO_MAX_LEN (511)
char GPSDOprocessingBuffer[GPSDO_MAX_LEN+1];
int GPSDOprocessingBufferIdx = 0;
unsigned long timeout = 0;

int GPSDOreadMessage(void)
{
  GPSDOprocessingBufferIdx = 0;
  GPSDOprocessingBuffer[0] = 0;
  if(GPSDOcount == 0)
  {
    return GPSDOprocessingBufferIdx;
  }

  volatile uint32_t snapshot = GPSDOcount;
agn:
  timeout = millis();
  snapshot = GPSDOcount;
  while ((millis() - timeout) < 10)
  {
    if(snapshot != GPSDOcount)
    goto agn;
  }
  size_t cpycount = GPSDOcount;

  memcpy(GPSDOprocessingBuffer, (const void*)GPSDObuffer, cpycount);
  GPSDOprocessingBufferIdx = GPSDOcount;
  GPSDOcount = 0;

  GPSDOprocessingBuffer[GPSDOprocessingBufferIdx] = 0;
  
  return GPSDOprocessingBufferIdx;
}


struct minmea_sentence_gga* GPSDOgetGPSGGA(char* data, int datalen)
{
   static struct minmea_sentence_gga frame;
   char* cptr;
   if(data == NULL)
        return NULL;

   data = strstr(data, "$GPGGA,"); //drop all except time frames
   cptr = strchr(data, '\n');
   if(cptr != NULL)
   {
    *cptr = 0;
   }
        

    switch (minmea_sentence_id(data, false)) {

        case MINMEA_SENTENCE_GGA: {
            if (minmea_parse_gga(&frame, data)) {
                return &frame;
            }
            else {
                return NULL;
            }
        } break;

        case MINMEA_INVALID: {
            return NULL;
        } break;

        default: {
            return NULL;
        } break;
    }
}



// enumerating 3 major temperature scales
enum {
  T_KELVIN=0,
  T_CELSIUS,
  T_FAHRENHEIT
};

// manufacturer data for B57045K 10k thermistor
// simply delete this if you don't need it
// or use this idea to define your own thermistors
#define B57045K_10k 4300.0f,298.15f,10000.0f  // B,T0,R0

// Temperature function outputs float , the actual
// temperature
// Temperature function inputs
// 1.AnalogInputNumber - analog input to read from
// 2.OuputUnit - output in celsius, kelvin or fahrenheit
// 3.Thermistor B parameter - found in datasheet
// 4.Manufacturer T0 parameter - found in datasheet (kelvin)
// 5. Manufacturer R0 parameter - found in datasheet (ohms)
// 6. Your balance resistor resistance in ohms  

float Temperature(int AnalogInputNumber,int OutputUnit,float B,float T0,float R0,float R_Balance)
{
  float R,T;

//  R=1024.0f*R_Balance/float(analogRead(AnalogInputNumber)))-R_Balance;
  R=R_Balance*(1024.0f/float(analogRead(AnalogInputNumber))-1);

  T=1.0f/(1.0f/T0+(1.0f/B)*log(R/R0));

  switch(OutputUnit) {
    case T_CELSIUS :
      T-=273.15f;
    break;
    case T_FAHRENHEIT :
      T=9.0f*(T-273.15f)/5.0f+32.0f;
    break;
    default:
    break;
  };

  return T;
}


unsigned long lastTempTime = 0;
unsigned long reportStamp = 0;
int ret;
struct minmea_sentence_gga* GPSDOframe = NULL;
float PAtemp;
int PAraw = 0;
void loop()
{

  if((millis() - lastTempTime) > 1000)
  {
    sprintf(GPSDOprocessingBuffer, "PA Temp.: %c%2d %cC",
      (PAtemp>=0)?' ':'-',
      (int)abs(PAtemp),
      (char)223);
    lcd.setCursor(0, 1);
    lcd.print(GPSDOprocessingBuffer);
    lastTempTime = millis();

    #ifdef DALLAS
    // načtení informací ze všech připojených čidel na daném pinu
    PAtemp = senzoryDS.getTempCByIndex(0);
    senzoryDS.requestTemperatures();
    //delay(300);
    #else
    PAraw = analogRead(A0); //READ A0
    PAtemp = Temperature(PAraw,T_CELSIUS,B57045K_10k,10000.0f);
    #endif
  }

  
  
  ret = GPSDOreadMessage();
  if(ret)
  {
    GPSDOframe = GPSDOgetGPSGGA(GPSDOprocessingBuffer, ret);
    if(GPSDOframe != NULL)
    {
      sprintf(GPSDOprocessingBuffer, "UTC: %02d:%02d:%02d %02d",
        GPSDOframe->time.hours,
        GPSDOframe->time.minutes,
        GPSDOframe->time.seconds,
        GPSDOframe->satellites_tracked);
      lcd.setCursor(0, 0);
      lcd.print(GPSDOprocessingBuffer);
    }
  }
#if 0
  if(GPSDOframe || ((millis()-reportStamp) > 2000))
  {
    /// PC REPORT ///

    //clear the page
    Serial.print((char)27);   //Print "esc"
    Serial.print("[2J");
    Serial.println("--- GPSDO Display ---");
    {
      float f;

      if(GPSDOframe)
      {
        Serial.println("GPS Data:");
        sprintf(GPSDOprocessingBuffer, "- Time: %02d:%02d:%02d\r\n",
          GPSDOframe->time.hours,
          GPSDOframe->time.minutes,
          GPSDOframe->time.seconds);
        Serial.print(GPSDOprocessingBuffer);
        f = minmea_tocoord(&GPSDOframe->longitude);
        sprintf(GPSDOprocessingBuffer, "- Longitude: %d.%04d\r\n", (int)f, (int)(1000*(abs(f)-abs((int)f))));
        Serial.print(GPSDOprocessingBuffer);
        f = minmea_tocoord(&GPSDOframe->latitude);
        sprintf(GPSDOprocessingBuffer, "- Latitude: %d.%04d\r\n", (int)f, (int)(1000*(abs(f)-abs((int)f))));
        Serial.print(GPSDOprocessingBuffer);
        sprintf(GPSDOprocessingBuffer, "- Fix Quality: %d\r\n", GPSDOframe->fix_quality);
        Serial.print(GPSDOprocessingBuffer);  
        sprintf(GPSDOprocessingBuffer, "- Satelites Tracked: %d\r\n", GPSDOframe->satellites_tracked);
        Serial.print(GPSDOprocessingBuffer); 
        f = minmea_tofloat(&GPSDOframe->hdop);
        sprintf(GPSDOprocessingBuffer, "- hdop: %d.%04d\r\n", (int)f, (int)(1000*(abs(f)-abs((int)f))));
        Serial.print(GPSDOprocessingBuffer); 
        f = minmea_tofloat(&GPSDOframe->altitude);
        sprintf(GPSDOprocessingBuffer, "- Altitude: %d.%04d %c\r\n", (int)f, (int)(1000*(abs(f)-abs((int)f))), GPSDOframe->altitude_units);
        Serial.print(GPSDOprocessingBuffer); 
        f = minmea_tofloat(&GPSDOframe->height);
        sprintf(GPSDOprocessingBuffer, "- Height: %d.%04d %c\r\n", (int)f, (int)(1000*(abs(f)-abs((int)f))), GPSDOframe->height_units);
        Serial.print(GPSDOprocessingBuffer);
        sprintf(GPSDOprocessingBuffer, "- DGPS Age: %d\r\n", GPSDOframe->dgps_age);
        Serial.print(GPSDOprocessingBuffer); 
      }
    }
    Serial.println("System Data:");
    sprintf(GPSDOprocessingBuffer, "- Uptime: %d s\r\n", millis()/1000);
    Serial.print(GPSDOprocessingBuffer);   
    sprintf(GPSDOprocessingBuffer, "- PA Temperature: %c%2d deg. C\r\n",
      (PAtemp>=0)?' ':'-',
      (int)abs(PAtemp));
    Serial.print(GPSDOprocessingBuffer);   

    GPSDOframe = NULL;
    reportStamp = millis();
  }
  #endif
}

     // Serial.write(c);
     // lcd.print(c);
     //lcd.setCursor(0, 1);

